const db = require('../database.js');
const pool = db.getPool();
module.exports = {
    addCoin: function (id, count) {
        pool.query(`SELECT user FROM coins WHERE EXISTS(SELECT user FROM coins WHERE user=${id})`, function (err, result) {
            if (err) console.log(err);
            if (!count) {
                count = 1;
            }
            if (Object.keys(result).length !== 0) {
                pool.query(`UPDATE coins SET coins = coins + ${count} WHERE user=${id}`, function (err) {
                    if (err) console.log(err);
                })
            } else {
                pool.query(`INSERT INTO coins (user, coins) VALUES (${id}, ${count})`, function (err) {
                    if (err) console.log(err);
                })
            }
        });
    },
    getCoins: function (id, callback) {
        pool.query(`SELECT coins FROM coins WHERE user=${id}`, function (err, result) {
            if (err) console.log(err);
            if (typeof result === 'undefined') return;
            if (Object.keys(result).length !== 0) {
                callback(result[0].coins);
            } else {
                callback('0');
            }
        });
    },
    removeCoins: function (id, count, callback) {
        pool.query(`UPDATE coins SET coins = coins - ${count} WHERE user=${id}`, function (err, result) {
            if (err) {
                console.log(err);
                callback(false);
            }
            callback(true);
        })
    }
};