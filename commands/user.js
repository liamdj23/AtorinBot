const Discord = require('discord.js')
module.exports = exports = function (extras) {
    return new Promise(function(resolve, reject){
        let langdir = __dirname.replace('commands', 'languages/')
        const lang = require(langdir + extras[3] + '.js')
        let message = extras[0]
        let client = extras[1]
        let args = [2]
        if (args.length === 0) {
            resolve([lang.user.usage])
            return
        }
        if (typeof message.mentions.users.first() === 'undefined') {
            resolve([lang.user.usage])
            return
        }
        let user = message.mentions.users.first()
        message.guild.fetchMember(user).then(member => {
            const embed = new Discord.RichEmbed()
            embed.setAuthor(lang.user.title + user.username, client.user.avatarURL)
            embed.setThumbnail(user.avatarURL)
            embed.addField(lang.user.joined, member.joinedAt, true)
            embed.addField(lang.user.muted, member.mute, true)
            embed.addField(lang.user.role, member.roles.last().name)
            embed.addField("ID:", member.id, true)
            embed.setColor(member.roles.last().color)
            resolve([embed])
        })
    })
}