const Discord = require('discord.js')
module.exports = exports = function (extras) {
  let langdir = __dirname.replace('commands', 'languages/')
  const lang = require(langdir + extras[3] + '.js')
  return new Promise(function (resolve, reject) {
    const embed = new Discord.RichEmbed()
    embed.setAuthor('👛 ' + lang.flip.title, extras[1].user.avatarURL)
    embed.addField(lang.flip.result, flip())
    embed.setFooter('AtorinBot by LiamxDev')
    embed.setColor('ff4949')
    resolve([embed])
  })
  function flip () {
    let value = Math.round(Math.random() * 1)
    if (value === 0) {
      return lang.flip.heads
    } else if (value === 1) {
      return lang.flip.tails
    } else {
      return lang.flip.retry
    }
  }
}
