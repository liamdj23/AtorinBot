const Discord = require('discord.js')
const wtc = require('what-the-commit')
module.exports = exports = function(extras){
    return new Promise (function (resolve, reject) {
        let langdir = __dirname.replace('commands', 'languages/')
        const lang = require(langdir + extras[3] + '.js')
        let client = extras[1]
        wtc().then(commit => {
            const embed = new Discord.RichEmbed()
            embed.setAuthor(lang.wtc.title, client.user.avatarURL)
            embed.setDescription(`git commit -m "${commit.replace('\n', '')}"`)
            embed.setFooter('AtorinBot by LiamxDev')
            embed.setColor('e26d14')
            resolve([embed])
        })
    })
}