const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject, reaction) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.poll.usage])
      return
    }
    const embed = new Discord.RichEmbed()
    embed.setAuthor('❓ ' + lang.poll.title + extras[0].author.username, extras[1].user.avatarURL)
    embed.addField(lang.poll.question, extras[0].content.replace('&poll ', ''))
    embed.setFooter('AtorinBot by LiamxDev')
    embed.setColor('dbb94a')
    resolve([embed, ['👍', '👎']])
  })
}
