const Discord = require('discord.js')
const db = require('./../database.js')
let pool = db.getPool()
module.exports = exports = function (extras) {
    return new Promise(function (resolve, reject) {
        let langdir = __dirname.replace('commands', 'languages/')
        const lang = require(langdir + extras[3] + '.js')
        let message = extras[0]
        let client = extras[1]
        let args = extras[2]
        if (!message.member.hasPermission('ADMINISTRATOR')) {
            resolve([lang.logs.permission + 'ADMINISTRATOR'])
            return
        }
        let channelId = message.guild.channels.find('name', message.channel.name)
        pool.query(`UPDATE servers SET logs = '${channelId.toString().replace(/[#<>]/g, '')}' WHERE server = ${message.guild.id}`, function (err, result) {
            if (err) {
                resolve([lang.logs.fail])
                throw err
            }
            resolve([lang.logs.success])
        })
    })
}
