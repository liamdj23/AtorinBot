const db = require('./../database.js')
const pool = db.getPool()
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let message = extras[0]
    // let client = extras[1]
    let args = extras[2]
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (args.length === 0) {
      resolve([lang.lang.usage])
      return
    }
    if (args[0] !== 'pl' && args[0] !== 'en') {
      resolve([lang.lang.usage])
      return
    }
    if (!message.member.hasPermission('ADMINISTRATOR')) {
      resolve([lang.lang.permission + 'ADMINISTRATOR'])
      return
    }
    pool.query(`UPDATE servers SET language = '${args[0]}' WHERE server = ${message.guild.id}`, function (err, result) {
      if (err) {
        resolve([lang.lang.fail])
        throw err
      }
      resolve([lang.lang.success + args[0]])
    })
  })
}
