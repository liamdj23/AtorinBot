const coins = require('../utils/coins');
module.exports = exports = function (extras) {
    return new Promise(function(resolve){
        let langdir = __dirname.replace('commands', 'languages/');
        const lang = require(langdir + extras[3] + '.js');
        let message = extras[0];
        coins.getCoins(message.author.id, function (count) {
            resolve([`${message.author} ` + lang.coins.message + count]);
        });
    })
};